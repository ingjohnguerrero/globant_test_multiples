//
//  test_globant_tableTests.m
//  test_globant_tableTests
//
//  Created by John Edwin Guerrero Ayala on 9/5/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CheckedNumber.h"

@interface test_globant_tableTests : XCTestCase

@end

@implementation test_globant_tableTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(void)testMultipleOfTwoTrueAssert{
    XCTAssertTrue([CheckedNumber isMultipleNumberMultipleOfTwo:2]);
}

-(void)testMultipleOfTwoFalseAssert{
    XCTAssertFalse([CheckedNumber isMultipleNumberMultipleOfTwo:5]);
}

-(void)testMultipleOfThreeTrueAssert{
    XCTAssertTrue([CheckedNumber isMultipleNumberMultipleOfThree:3]);
}

-(void)testMultipleOfThreeFalseAssert{
    XCTAssertFalse([CheckedNumber isMultipleNumberMultipleOfThree:5]);
}

-(void)testMultipleOfTwoTypeTrueAssert{
    XCTAssertEqual(multipleOf2,[CheckedNumber checkMultipleTypeOf:2]);
}

-(void)testMultipleOfThreeTypeTrueAssert{
    XCTAssertEqual(multipleOf3,[CheckedNumber checkMultipleTypeOf:3]);
}

-(void)testMultipleOfBothTypeTrueAssert{
    XCTAssertEqual(multipleOf2n3,[CheckedNumber checkMultipleTypeOf:6]);
}

-(void)testMultipleOfNoMultipleAssert{
    NumberMultipleType resultOfCheck = [CheckedNumber checkMultipleTypeOf:5];
    XCTAssertEqual(noMultipleAvailable, resultOfCheck);
}

@end
