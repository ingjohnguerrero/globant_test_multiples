//
//  CheckedNumber.h
//  test_globant_table
//
//  Created by John Edwin Guerrero Ayala on 9/5/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//
typedef enum numberType {
    multipleOf2 = 1,
    multipleOf3,
    multipleOf2n3,
    noMultipleAvailable
} NumberMultipleType;

#import <Foundation/Foundation.h>

@interface CheckedNumber : NSObject

@property (nonatomic, assign) NSInteger number;
@property (nonatomic, assign) NumberMultipleType numberType;

-(instancetype)initWithNumber:(NSInteger)numberToCheck;
+(BOOL)isMultipleNumberMultipleOfTwo:(NSInteger) number;
+(BOOL)isMultipleNumberMultipleOfThree:(NSInteger) number;
+(NumberMultipleType)checkMultipleTypeOf:(NSInteger) number;

@end
