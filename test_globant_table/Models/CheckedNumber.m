//
//  CheckedNumber.m
//  test_globant_table
//
//  Created by John Edwin Guerrero Ayala on 9/5/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "CheckedNumber.h"

@implementation CheckedNumber

-(instancetype)initWithNumber:(NSInteger)numberToCheck{
    self = [super init];
    if(self)
    {
        _number = numberToCheck;
        _numberType = [[self class] checkMultipleTypeOf:numberToCheck];
    }
    return self;
}

+(BOOL)isMultipleNumberMultipleOfTwo:(NSInteger) number{
    if (number % 2 == 0) {
        return YES;
    }else{
        return NO;
    }
}

+(BOOL)isMultipleNumberMultipleOfThree:(NSInteger) number{
    if (number % 3 == 0) {
        return YES;
    }else{
        return NO;
    }
}

+(NumberMultipleType)checkMultipleTypeOf:(NSInteger) number{
    BOOL multipleOf2Check = [[self class] isMultipleNumberMultipleOfTwo:number];
    BOOL multipleOf3Check = [[self class] isMultipleNumberMultipleOfThree:number];
    if (multipleOf2Check && multipleOf3Check) {
        return multipleOf2n3;
    }else if (multipleOf3Check){
        return multipleOf3;
    }else if (multipleOf2Check){
        return multipleOf2;
    }else{
        return noMultipleAvailable;
    }
}

@end
