//
//  NumberCheckTableViewCell.m
//  test_globant_table
//
//  Created by John Edwin Guerrero Ayala on 9/5/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "NumberCheckTableViewCell.h"

@implementation NumberCheckTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (NumberCheckTableViewCell*)buildWithOwner:(id)owner {
    return [[[NSBundle mainBundle] loadNibNamed:@"NumberCheckTableViewCell" owner:owner options:nil] objectAtIndex:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
