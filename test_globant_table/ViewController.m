//
//  ViewController.m
//  test_globant_table
//
//  Created by John Edwin Guerrero Ayala on 9/5/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController{
    NSMutableArray *evaluatedNumberArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    evaluatedNumberArray = [NSMutableArray new];
    [self calculateAllNumbers];
    
    NSString *cellClassName = @"NumberCheckTableViewCell";
    
    [_mainTableView setDelegate:self];
    [_mainTableView registerNib:[UINib nibWithNibName:cellClassName bundle:nil]  forCellReuseIdentifier:@"CheckerCell"];
    // Do any additional setup after loading the view, typically from a nib.
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [evaluatedNumberArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *reusableIdentifier = @"CheckerCell";
//    NumberCheckTableViewCell *cell = (NumberCheckTableViewCell *)[_mainTableView dequeueReusableCellWithIdentifier:resusableIdentifier];
    NumberCheckTableViewCell * cell = [_mainTableView dequeueReusableCellWithIdentifier:reusableIdentifier forIndexPath:indexPath];
    CheckedNumber *currentNumber = [evaluatedNumberArray objectAtIndex:indexPath.row];
    NSString *numberString =  [NSString stringWithFormat:@"%ld", (long)[currentNumber number] ];
//    if (!cell) {
//        cell = [_mainTableView dequeueReusableCellWithIdentifier:resusableIdentifier forIndexPath:indexPath];
//    }
    switch ([currentNumber numberType]) {
        case multipleOf2:
            [[cell cellLabel] setTextColor:[UIColor redColor]];
            numberString = [NSString stringWithFormat:@"..........%@", numberString];
            [[cell cellLabel] setTextAlignment:NSTextAlignmentLeft];
            break;
        
        case multipleOf3:
            [[cell cellLabel] setTextColor:[UIColor blueColor]];
            [[cell cellLabel] setTextAlignment:NSTextAlignmentCenter];
            break;
        case multipleOf2n3:{
            //Hot pink color 255, 0, 102
            CGFloat cellRed = 255/255.0f;
            CGFloat cellGreen = 0/255.0f;
            CGFloat cellBlue = 102/255.0f;
            [[cell cellLabel] setTextColor:[UIColor colorWithRed:cellRed green:cellGreen blue:cellBlue alpha:1]];
            numberString = [NSString stringWithFormat:@"%@..........", numberString];
            [[cell cellLabel] setTextAlignment:NSTextAlignmentRight];
        }
            break;
        default:
            [[cell cellLabel] setTextAlignment:NSTextAlignmentLeft];
            [[cell cellLabel] setTextColor:[UIColor blackColor]];
            break;
    }
    [[cell cellLabel] setText:numberString];
    return cell;
}

-(void)calculateAllNumbers{
    NSInteger i;
    for (i = 0; i < 100; i++) {
        CheckedNumber *tempNumber = [[CheckedNumber alloc] initWithNumber:[[NSString stringWithFormat:@"%ld", (long)i] integerValue]];
        [evaluatedNumberArray setObject:tempNumber atIndexedSubscript:i];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
