//
//  ViewController.h
//  test_globant_table
//
//  Created by John Edwin Guerrero Ayala on 9/5/16.
//  Copyright © 2016 John Edwin Guerrero Ayala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CheckedNumber.h"
#import "NumberCheckTableViewCell.h"


@interface ViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *mainTableView;


@end

